
from rich import print
from tools import *
from corpus import SOURCES
import os
import sys
from alive_progress import alive_bar, config_handler
config_handler.set_global(file=sys.stderr)

BASE_PATH = 'archive'
README = 'README.md'

README_TEXT = '''
# {title}

**{motto}**

![]({image}){{width=500px}}

This dataset is an archive of the **"{title}"** corpus
({content}, metadata).
It provides both raw data and data for integration
with the Dezrann music web platform: <https://www.dezrann.net/explore/{id}>.

{text}

{availability}

**License:** {license}

**Maintainers:** {maintainer}

**References**

- {ref_pub}
- <https://dx.doi.org/{ref_doi}>
- <{ref}>

**Dataset content**

- `{id}.json`. Main archive catalog, with metadata, as described on <https://doc.dezrann.net/metadata>
{content_detail}
'''


def archive_json(corpus, args):
    
    print(f'[green]### Archiving {corpus}')
    failed = []

    exec(f'rm -rf {corpus.path_archive} {corpus.path_archive}.zip', dry=not args.go)
    downloaded = 0
    sources = set()
    
    ## Remove templates
    if 'template' in corpus.d:
        del corpus.d['template']
    
    ## Walk through all metadata
    with alive_bar(len(list(walk_items(corpus.d))), file=sys.stderr, enrich_print=False) as bar:
      for (path, parent, k, v) in walk_items(corpus.d):
        bar.text = path
        if str(v).startswith('http') or str(v).startswith('file:'):
            if k.startswith('git') or k.startswith('ref') or k.endswith('ref'):
                bar()
                continue
            print(path, v)
            
            if ':' in k:
                path_k = k[:k.index(':')]
            else:
                path_k = k
            sources.add(path_k)
    
            dpath = f'{corpus.path_archive}/{path_k}'
            # dpath = PATH
            
            os.makedirs(dpath, exist_ok=True)
            
            local = get_file_or_url(v, path=dpath, dry=not args.go)
            if local:
                parent[k] = local.replace(f'{corpus.path_archive}/', '')
                downloaded += 1
                if args.limit and downloaded >= args.limit:
                    print('[purple]! Stop here, limit reached')
                    break                    
            else:
                parent[k] = ""
                failed += [path]
        bar()
                
    print()
    print(f'==> [green]{downloaded} files ok[/]')
    if failed:
        print(f'==> [red]{len(failed)} files failed')
        for f in failed:
            print(f'[red]   {f}')
    
    ## Save .json
    json_archive = f'{corpus.path_archive}/{corpus.ID}.json'
    corpus.save(target=json_archive)

    ## What is this content?
    ss = []
    ss_detail = []
    for k, v in SOURCES.items():
        if k in sources:
            vv, vvv = v
            ss.append(vv)
            ss_detail.append(f"- `{k}/`: {vvv}")
    content = ', '.join(ss)
    content_detail = '\n'.join(ss_detail)
    return content, content_detail

def archive_readme(corpus, content, content_detail):
    ## Creates README.md
    readme = f'{corpus.path_archive}/{README}'
    meta = MetaDict(corpus.d['corpus'])
    meta.update(meta['contributors'])   
    meta['content'] = content
    meta['content_detail'] = content_detail
        
    print(f'[yellow]==> {readme}')
    with open(readme, 'w') as f:
        f.write(README_TEXT.format_map(meta))

    return readme

def archive(corpus, args):
    corpus.path_archive = f'{BASE_PATH}/{corpus.ID}'

    content, content_detail = archive_json(corpus, args)
    readme = archive_readme(corpus, content, content_detail)

    ## Compress
    exec(f'cd {BASE_PATH}; zip -r {corpus.ID}.zip {corpus.ID}/')

    ## Convert to html
    html = f'{corpus.path_archive}/README.html'
    exec(f'pandoc {readme} > {html}')

