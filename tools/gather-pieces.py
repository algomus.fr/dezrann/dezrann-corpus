

import glob 
import json

def gather(corpus, extra=None):

    path = f'corpus/{corpus}/*/info.json'
    print('<==', path)
    
    pieces = {}    
    for f in glob.glob(path):
        
        j = json.load(open(f))
        if extra:
            j.update(extra)
        
        pieces[j['id']] = j 
    
    print(f'==> {len(pieces)} pieces')
    d = { 'pieces': pieces}
    
    ff = f'metadata/{corpus}-pieces.json'
    print('==>', ff)
    with open(ff, 'w') as fff:
        json.dump(d, fff, indent=2)
    
    
gather('supra',
       {'quality:audio': '3',
        'quality:audio:synchro': '3'})