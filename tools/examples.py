

from doctest import Example


import os

SRC = 'corpus/'
TARGET = 'doc/examples/'

EXAMPLES = [
    'vivaldi/spring-1', 
    'mozart-piano-sonatas/k279.1',
    'mozart-symphonies/k425.1',
]

for ex in EXAMPLES:
    src = SRC + ex + '/info.json'
    target = TARGET + ex.replace('/', '-') + '+info.json'
    os.system(f'cp -v {src} {target}')