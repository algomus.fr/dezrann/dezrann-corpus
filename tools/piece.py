
from tools import *
from rich import print

import dezrann_api
import os.path

INFO_JSON = 'info.json'
SYNCHRO_JSON = 'synchro.json'

class Piece():

    def __init__(self, corpus, piece_id, dry, prod, host, archive=False):
        self.dry = dry
        self.prod = prod
        self.host = host
        self.corpus = corpus
        self.d = corpus[piece_id]
        self.path = 'corpus/' + self.corpus.ID + '/'
        self.path_in = corpus.PATH_IN if hasattr(corpus, 'PATH_IN') else ''
        self.id = os.path.basename(self.d['opus']['id']) if 'id' in self.d['opus'] else piece_id

        self.id_on_server = self.id.replace(':', '-') # before dezrann-back#178
        self.id = self.id_on_server
        self.path_on_server = self.corpus.ID + '/' + self.id_on_server

        title = self.d['opus']['title'] if 'title' in self.d['opus'] else self.d['opus']['piece:title']
        if 'movement:title' in self.d['opus']:
            title += ' / ' + self.d['opus']['movement:title']

        print(f"[yellow]{self.id} ➡️ {title}")

        self.scores = []
        self.images = []
        self.positions = None
        self.f_score = None
        self.f_mmap = None
        self.mmap = None
        self.audios = []
        self.analyses = []
        self.neuma = None

        audio_index = 1
        if not 'sources' in self.d:
            self.d['sources'] = {}
            print('[red]! No sources')
            return
        
        # Main loop, iterate over sources
        for s in self.d['sources']:
            if 'score' in s:
                self.scores += [s]
                if archive:
                    # We just want to process scores
                    continue
                if 'measure-map' in s:
                    self.mmap = s['measure-map']
            if archive:
                continue
            if 'score:neuma' in s:
                self.neuma = s['score:neuma']
            if ('audio' in s and s['audio']) or 'audio:yt' in s or 'video:yt' in s:
                if 'synchro' in s and s['synchro']:
                    s['synchro'] = get_file_or_url(s['synchro'], f'''synchro-{audio_index}.json''', path=self.path_in)
                if 'audio' in s and ('audio:yt' in s or 'video:yt' in s):
                    del s['audio']
                self.audios += [s]
                audio_index += 1
            if 'analysis' in s:
                self.analyses += [get_file_or_url(s['analysis'], path=self.path_in)]
            if 'image' in s and 'positions' in s:
                self.positions = get_file_or_url(s['positions'])
                self.images += [get_file_or_url(s['image'])]
            if 'images' in s and 'positions' in s:
                self.positions = get_file_or_url(s['positions'])
                for image in s['images']:
                    self.images += [get_file_or_url(image)]

        if archive:
            return

        self.analyses = list(filter(None, self.analyses))

        if 'analysis:default' in self.d:
            if self.d['analysis:default'] not in map(os.path.basename, self.analyses):
                print('! analysis:default not found:', self.d['analysis:default'])
                del self.d['analysis:default']

    def get_audio(self, key):
        '''
        Return some audio, indexed by 'video:yt', 'audio:yt', or 'audio'
        '''
        for audio in self.audios:
            for kkey in ['video:yt', 'audio:yt', 'audio']:
                if kkey in audio:
                    if os.path.basename(audio[kkey]) == key:
                        return audio
        raise KeyError

    def _updateYtIds(self, info) :
        youtubeVideoIds = [];
        if self.audios:
            for audio in self.audios:
                if 'video:yt' in audio:
                    youtubeVideoIds.append(audio['video:yt'])
                if 'audio:yt' in audio:
                    youtubeVideoIds.append(audio['audio:yt'])
        if len(youtubeVideoIds) > 0:
            if len(youtubeVideoIds) == 1 :
                info['ytId'] = youtubeVideoIds[0]
            else :
                info['ytId'] = youtubeVideoIds;

    def prepare_info(self):
        info = self.d.copy()
        del info['sources']
        if 'analyses' in info:
            del info['analyses']

        # Before back!82
        self._updateYtIds(info)

        self.info = INFO_JSON
        with open(self.info, 'w') as ff:
            json.dump(info, ff, indent=2)

    def prepare_score(self, with_xml_cleanup = False):
        assert(len(self.scores) == 1)
        f = get_file_or_url(self.scores[0]['score'], path=self.path_in)
        if f is None:
            raise Exception(f"Score not found: {self.scores[0]['score']}")

        # Musescore
        if '.msc' in f:
            f = dezrann_api.mscore_to_xml(f, f.replace('.mscx', '').replace('.mscz', ''), dry=self.dry)

        if 'settings' in self.d:
            if 'fix-krn-to-musicxml' in self.d['settings'] and self.d['settings']['fix-krn-to-musicxml']:
                ff = f.replace('.krn', '.musicxml')
                exec(f'python3 ../../../corpus/mozart-string-quartets/krn-to-musicxml.py {f} {ff}',
                    dry=self.dry)
                f = ff
            if 'fix-id' in self.d['settings'] and self.d['settings']['fix-id']:
                ff = f.replace('.mei', '_id.mei')
                try:                   
                    exec(f'python3 ../../../corpus/ecolm-lute/add_ids_to_mei_notes.py {f} {ff}',
                     dry=self.dry)
                    f = ff
                except:
                    print('! Error in add_ids')

        if f.endswith(('.mxl', '.musicxml', '.xml')) and with_xml_cleanup :
            print('clean up...')
            dezrann_api.xml_cleanup(f, dry=self.dry)

        # Compressed, before back#121
        # '.krn' also
        if '.mxl' in f or '.musicxml' in f or '.krn' in f:
            f = dezrann_api.to_mei(
                f,
                f.replace('.mxl', '')
                 .replace('.musicxml', '')
                 .replace('.krn', ''),
                dry=self.dry
            )

        # Before back#108
        base, ext = os.path.splitext(f)
        path, _ = os.path.split(base)
        if path:
            ff = path + '/' + self.id + ext
        else:
            ff = self.id + ext
        f = os.path.normpath(f)
        ff = os.path.normpath(ff)
        if f != ff:
            exec(f'cp -v "{f}" "{ff}"', dry=self.dry)
            
        self.f_score = ff
    
    def publish(self):
        # Before back!82
        f_audio = []
        for index, audio in enumerate(self.audios):
            if 'audio' in audio:
                ext = os.path.splitext(audio['audio'])[1]
                filename = get_file_or_url(audio['audio'], f'{self.id}-{index}{ext}', path=self.path_in)
                try:
                    f_audio.append(dezrann_api.to_mp3(filename))
                except:
                    pass
    
        dezrann_api.publish(self.corpus.ID, self.dry,
                            f_score=self.f_score, 
                            f_audio=f_audio, 
                            f_info=self.info,
                            host=self.host)


    def time_sig_from_measure_map(self):
        # back#147
        d = json.load(open(self.f_mmap))

        # Remove duplicates, keeping the order
        tss = []
        for m in d:
            ts = m['time_signature']
            if ts and ts not in tss:
                tss += [ts]

        if len(tss) > 3:
            tss = tss[:3] + ['…']
        return ', '.join(tss)

    def publish_measure_map(self, only_info=False):
        # Before front#473
        if self.mmap and self.mmap == 'GEN':
            self.mmap = dezrann_api.create_mmap(self.f_score, dry=False)        
        if self.mmap and not self.f_mmap:
                self.f_mmap = get_file_or_url(self.mmap, path=self.path_in)
        if self.f_mmap is None:
            return

        url = dezrann_api.publish_static(self.path_on_server, self.f_mmap,
                                         dry=only_info, prod=self.prod, host=self.host)
        self.d['opus']['measure-map'] = url

        try:
            self.d['opus']['meter'] = self.time_sig_from_measure_map()
        except:
            print('! time_sig_from_measure_map() failed')


    def publish_images(self):
        for image in self.images:
            dezrann_api.publish_image(self.path_on_server,
                                      image, self.positions,
                                      dry=self.dry, prod=self.prod, host=self.host)

    def publish_analyses(self):
        dezrann_api.publish_analyses(self.path_on_server, self.analyses,
                                     dry=self.dry, prod=self.prod, host=self.host)

    def update_synchro(self):
        index = 0
        indexyt = 0
        for audio in self.audios:
            if 'synchro' in audio:
                if 'audio:yt' in audio or 'video:yt' in audio:
                    ind = '%s-yt' % indexyt
                    indexyt += 1
                else:
                    ind = str(index)
                    index += 1
                dezrann_api.update_synchro(self.path_on_server, self.id_on_server, 
                                           ind, audio['synchro'],
                                           dry=self.dry, prod=self.prod, host=self.host)

    def update_metadata_local(self, local_info, dry=False):
        '''
        Update a local info.json file (coming from the server)
        with this piece metadata
        '''

        with UpdateJson(local_info, dry=dry, sort=False) as j:
            j['id'] = self.id
            j['opus'].update(self.d['opus'])
            for k in self.d:
                if 'quality' in k:
                    j[k] = self.d[k]
            if 'settings' in self.d:
                if not 'settings' in j:
                    j['settings'] = {}
                j['settings'].update(self.d['settings'])
            if 'audios' in j['sources']:
              for source in j['sources']['audios']:
                try:
                    if 'yt-id' in source:
                        path = source['yt-id']
                    else:
                        # We hope it's the good file...
                        path = source['file'].replace('-0', '')
                    audio = self.get_audio(path)
                    source.update(audio)
                    for im in source['images']:
                        im.update(audio)
                    print(audio)
                except:
                    print('! no update for', source)

            ###
            if 'images' in j['sources']:
                for source in j['sources']['images']:
                    try:
                        if source['type'] == 'score':
                            sco = self.scores[0]
                            if not 'ref' in sco:
                                sco['ref'] = sco['score']
                            source.update(sco)
                            print(source)
                    except:
                        print('! no update for score', score)

            ### 
            for image in self.images:
                j['sources']['images'] += [{
                    'image': image,
                    'type': '3d',
                    'name': 'Scan'
                }]
                break

            ### Hard-coded, CollabScore
            if self.neuma:
                for source in j['sources']['images']:
                    if not 'name' in source:
                        continue
                    if source['name'] == 'vector':
                        source['name'] = "Partition de référence, linéaire"
                        source['contributors'] = { "editor": "IReMUS / CollabScore" }
                    elif source['name'] == '3D Score':
                        source['name'] = "Partition de référence, mise en page"
                        source['contributors'] = { "editor": "IReMUS / CollabScore" }
                        source['source'] = 'http://neuma.huma-num.fr/home/opus/%s/' % self.neuma
                    elif source['name'] == '3D Scan':
                        source['name'] = "Partition imprimée (BnF)"
                        source['contributors'] = { "editor": "IRISA / CollabScore" }
    


    def __str__(self):
        return str(self.d)

def process(corpus, piece_id, args, update_meta=False):
        piece = Piece(corpus, piece_id,
                      dry=args.dry, prod=args.prod, host=args.host,
                      archive=args.archive)

        if update_meta:
            piece.update_metadata_local('info.json', dry=args.dry)
            return True
        
        if not piece.d['sources']:
            return False

        if len(piece.scores):
            piece.prepare_score(args.cleanup_xml)

        # after prepare_score(), but before prepare_info()
        piece.publish_measure_map(only_info=True)

        piece.prepare_info()

        if args.archive:
            # Do not continue, only return the (possibly converted) score
            if piece.f_score:
                # Hack to find back the file
                piece.scores[0]['score'] = f'file:../../tmp/{piece.f_score}'
            return True

        if args.pub:
            if piece.neuma:
                dezrann_api.generate_from_neuma(piece.neuma, dry=args.dry)
            else:
                piece.publish()

        piece.publish_measure_map() # after publish()
        piece.publish_images()

        try:
            piece.publish_analyses()
        except Exception as e:
            print("[red]! Analysis upload failed[/red]")

        try:
            piece.update_synchro()
        except Exception as e:
            print("[red]! Synchro upload failed[/red]")


        return True

