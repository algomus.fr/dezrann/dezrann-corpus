
import json
from rich import print

DEMO_CORPUS = 'test-corpus.json'
PATH = 'metadata/'

DEMOS = [
    ('bach-fugues.full.json', 'bwv862', 'fugue-bwv862', '.krn, .dez, video:yt, audio'),    
    ('edmus.json', 'debussy-mandoline', 'debussy-mandoline', '.mei'),
    ('slp.full.json', '237-4', 'slp-237.4', '.mxl'),
    # ('haydn-symphonies.json, 'XXX'),
    ('collabscore-saintsaens.full.json', 'all-collabscore-saintsaens-ref-C079_0', 'coccinelle-C079', 'Neuma, audio:yt'),
    ('mozart-piano-sonatas.full.json', 'k284.3', 'sonata-k284.3', '.mscx + .mm.json, audio:yt, .dez'),
    ('openscore-lieder.json', '4986023', 'mendelssohn-ersehnte', '.mcsz, .dez, audio:yt'),
    ('edmus.json', 'cantaloupe-island', 'hancock-cantaloupe', 'audio:yt'),
]

OUT = {
    'corpus' : {
        "id": "test",
        "motto": "🚧 Test pieces",
        "title": "🚧 Test pieces",
        "text": "Pieces from different corpora to test source specification and piece upload",
        "availability": "",
        "genre": "all",
        "quality": 1,
        "showcase": [      
    ]
    },
    'settings': {
        'access': 'algomus'
    },
    'pieces': {}
}

def postprocess(pieces):
    del pieces['coccinelle-C079']['sources'][3]
    del pieces['coccinelle-C079']['sources'][2]

    pieces['slp-237.4']['sources'][0]['score'] = "http://alb.algomus.net:8080/resources/corpus/scores/237-4.mxl"
    del pieces['slp-237.4']['sources'][0]['measure-map']

def out_demos():
    out = OUT
    for (corpus, piece, testname, description) in DEMOS:
        print(f'<== {corpus} [purple]{piece}')
        with open(PATH + corpus) as f:
            j = json.load(f)
            p = j['pieces'][piece].copy()
            # print(p)

            # We do not test here opus values, keep it to the minimum
            p['opus'] = {}            
            for k in j['pieces'][piece]['opus'].keys():
                if 'title' in k or 'variant:first' in k:
                   p['opus']['title'] = 'Test 🚧 ' + j['pieces'][piece]['opus'][k]

            p['opus']['collection'] = '🚧 ' + description

            out['pieces'][testname] = p

    postprocess(out['pieces'])

    ff = PATH + DEMO_CORPUS
    print(f'[yellow]==> {ff}')
    json.dump(out, open(ff, 'w'), indent=2, ensure_ascii=False)

out_demos()