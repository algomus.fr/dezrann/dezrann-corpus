

import json
import sys
from rich import print
from tools import *

TMP = 'neuma-tmp.json'
NEUMA_PUT = f'''curl -sX PUT "http://neuma.huma-num.fr/rest/collections/all/collabscore/saintsaens-ref/%s/_sources/"  -H 'Content-Type: application/json'   -d @{TMP}'''


OUT = '''{
    "ref": "yt-%d",
    "description": "%s",
    "source_type": "MPEG",
    "url":"https://www.youtube.com/watch?v=%s"
}
'''

def description(d):
    s = ''
    if 'contributors' in d:
        s += ', '.join(d['contributors'].values())
    if 'year' in d:
        s += f" ({d['year']})" 
    return s
    
def fill_neuma(d):
    for p, piece in d['pieces'].items():
        
        try:
            key = p.split(':')[-1]
            print(f"### [green] {key} [yellow] {piece['opus']['title']}")
        except:
            print('[red]! Neuma info failed', p)
            continue
        
        if key == 'C080_0':
            continue
        
        index = 0
        
        for source in piece['sources']:
            if not 'ytId' in source:
                continue
    
            index += 1
            desc = description(source)
            print(index, source['ytId'], '[green]', desc)

            with open(TMP, 'w') as f:
                f.write(OUT % (index, desc, source['ytId']))
                
            exec(NEUMA_PUT % key)

if __name__ == '__main__':
    d = json.load(open('metadata/collabscore-saintsaens.json'))
    fill_neuma(d)
            