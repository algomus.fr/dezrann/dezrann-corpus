

## Adding and maintaining a new corpus on Dezrann

A corpus on Dezrann should contain **score(s)**, 
set(s) of **analyses/annotations**, synchronized **audio(s)** 
(or at least two of the three), and appropriate **metadata**.
As far as possible, these files should be available under *open-data licenses*,
as described on [Open Science and Licenses](licenses).
We try to add corpus in a *reproducible way*, as for the [public corpora](rebuild)
available on the platform. Data you use or you create
(scores, annotations, audios, metadata) have to be available within a git or through any stable URL.

As a corpus curator/maintainer, your responsability is mostly to prepare, update, and maintain a *corpus description file* `metadata/xxxxxx.json` giving all the information and pointing to some sources. This involves the following steps.

### Step 1. Data/metadata preparation

*(Sources data)*

- Prepare *scores* (preferably .mei, but also .musicxml or other symbolic formats, processed with Verovio). See [Scores preparation](scores);
- Prepare *annotation data*. This may come either from external data and converted to the [json `.dez` format](dez-format) representing labels, or can be done later, once the scores are on Dezrann;
- Prepare *audio/video and synchronization* (preferably open-data audio/video, or possibly YouTube links). The [synchronization](synchro) can either come from external data, or can be done later, once the scores are on Dezrann;

*(Metadata)*

- Prepare the *corpus description file* `metadata/xxxxxx.json` combining  *corpus* and *piece* metadata, as described on [Specifying corpus and piece data and metadata](metadata).

### Step 2. First build of the corpus

When the `metadata/xxxxxx.json` file is ready:

- Either [contact us](mailto:contact@dezrann.net?subject=%5BDezrann%5D%20New%20corpus&body=%0D%0AHello%2C%0D%0A%0D%0AI%20would%20like%20to%20create%20a%20new%20corpus%20for%20Dezrann%3A%20Xxxxxx%0D%0A%0D%0A%0D%0A%0D%0A) with the corpus description file and/or open a MR with this file in the `/metadata` directory. As of Q1 2025, uploading the corpus on Dezrann now involves manual steps on the server, and the process is, so this the preferred step. Better tools are scheduled for Q3 2025.

- And/or, following examples on [how to rebuild public corpora](rebuild), build the corpus with the `tools/dezrann-corpus.py` script on a local or on a public Dezrann installation. Note that the corpus has to be built and checked on a local installation of Dezrann (or on the test server, contact us to have an account) and before upload to the production public server.


### Step 3. Check/curation

Once the corpus is on Dezrann, in the sandbox:

- Check *every* score, browsing each score until its end;
- When it was not done before, synchronize the audio files (see [synchro](synchro));
- Update data/metadata in the `metadata/xxxxxx.json` file, and to rebuild the corpus;
- In particular, fill/update quality values per piece, according to [quality](quality)
- List the problems encountered. If we do not manage to fix them, we may hide some scores of the corpus. This has to be documented in a text file, or better, for a corpus intended to be public, filled with [issues on the dezrann-corpus](https://gitlab.com/algomus.fr/dezrann/dezrann-corpus/-/issues) gitlab.


### Step 4. Publication and long-term maintainance

*(Communication, maintenance)*

- Check again corpus metadata, in particular the presentation material (`text`, `motto`, `availability`, `status`), with a few lines presenting the corpus;
- Contact us to finalize the publication/release;
- To improve again reproducibility, prepare with `tools/archive.py` a long-term archive (to be documented) and upload it on a institutional repository;
- After the corpus is published, update these data or metadata when needed.
