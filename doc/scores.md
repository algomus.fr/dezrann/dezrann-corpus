
### Preparing scores or other symbolic musical data for Dezrann

As of 2025, Dezrann use [Verovio](https://www.verovio.org) as its main rendering engine,
and thus can display any score (or tablature, or other source) that can be processed by Verovio.
Moreover, we use converter, using MuseScore or music21, to process more formats.
So, if `.mei` files are preferred, scores can also be given from formats such as `.musicxml`, `.krn`,  `.mscz`, or `.krn`.

The score files should be accessible on another git  (such as <https://github.com/musedata/humdrum-mozart-quartets/tree/main/kern>). When you are drafting your corpus, it's acceptable to first put such files on a relative path towards a private git or a shared cloud folder.

Ideally we should take existing scores "out of the box". If you change something, please trace modifications in a text file along the corpus.

**Check.** You can check on your side the rendering by installing and running verovio (`verovio foo.mei` or `verovio -a --breaks none foo.mei`) and/or online on <https://editor.verovio.org>. You will check every piece once it is on Dezrann.

