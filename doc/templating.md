
### Preparing piece metadata with templates

**Warning: as of Q1 2025, this information should be updated.**

The following steps are optional and help to produce a `metadata/xxxxx.json` file with templates.

1) In the git `dezrann-corpus`, add a new `metadata/*.py` file creating a new class,
taking example on [metadata/bach-fugues.py](../metadata/bach-fugues.py)
or on [metadata/mozart-string-quartets.py](../metadata/mozart-string-quartets.py).

`parse_keys` takes a filename (and/or an identifier), and return a dictionary of tokens, including `KEY` and `KEYS`.
For example, `parse_keys('k279.1.musicxml')` returns:

```
{ 
  'KEY': 'k279.1', 
  'KEYS': ['k279.1', 'k279', TEMPLATE], 
  'mvt': '1',
  'musicxmltitle': 'Allegro'
}
```

The `KEY` is the main identifier.  
The `KEYS` lists the identifiers from which metadata will be taken: it begins with the `KEY`, can include other identifiers, such as piece/collection identifiers, and ends with `TEMPLATE`.

There can be other custom tokens, either directly derived from the input data (here the `mvt`), or with some pre-processing: as an example, a `m21title` could be derived from data in the files.

2) Fill as possible the `metadata/xxxxx.json`, factorizing common things in the `template` section.
Take again example on [metadata/bach-fugues.json](../metadata/bach-fugues.json)
or on [metadata/mozart-string-quartets.json](../metadata/mozart-string-quartets.json).
See [doc/metadata.md](metadata.md) for the fields we are using.

3) append to the `AVAILABLE_CORPORA` in `metadata/corpora.py` and element corresponding to the new corpus taking example on [metadata/corpora.py](../metadata/corpora.py)

```
<className>('<xxxxx>','<xxxxx.json>',template=True)
```

4) **Check.** Launch, from `git/dezrann-corpus`, something like:

```bash
python3 tools/dezrann-corpus.py mozart-string-quartets --template --go  
```

It will populate a new json file : `metadata/mozart-string-quartets.full.json`. Check this file

5) **Commit** your changes on `corpus.py` on a new branch (`git checkout -b corpus-blabla`).

Push, create a merge request (MR). Assign this MR to Mathieu who checks that
and possibly ask for modifications before uploading everything to Dezrann.