

# Encoding music annotations with `.dez` files

Dezrann uses [json](http://en.wikipedia.org/wiki/JSON) files to encode an "analysis", that is a set of **labels** describing music annotations.
It is intended to be very small (at most a few kilobytes) and refers to the music notation, referring only to (symbolic) musical time and being independent of any audio file.
The `.dez` format makes no assumption on the annotation concepts, they are left to the user.

Here is such a `.dez` file:

```json
{  
  "labels": [
    { "type": "S", "start": 8.5, "ioi": 7.5, "staff": "1"},
    { "type": "CS1", "start": 8.25, "ioi": 7.75, "staff": "2"},
    { "type": "S", "start": 0.5, "ioi": 7.5, "staff": "2"},
  
    { "start": 8, "comment": "Second voice enters"},
    { "start": 16 },  
    { "type": "Pattern", "start": 16.5, "ioi": 1.5, "tag": "h", "layers": ["predicted"]},
    
    { "type": "Structure", "start": 16, "ioi": 8, "tag": "codetta"},
    { "type": "Pedal", "start": 114, "duration": 10, "tag": "Tonic pedal"},
    { "type": "Cadence", "start": 64, "tag": "v:PAC"}
  ],
  
  "meta":{
    "date": "2021-01-06T13:50:38.306Z",
    "producer": "Dezrann c6c80150",
    "title": "Fugue in C minor",
    
    "layout": [
      { "filter" : { "type": "Structure" }, "style" : {"line": "bot.1"} },
      { "filter" : { "type": "Pedal" }, "style" : {"line": "top.1", "color": "#bbbbbb"} },
      { "filter" : { "type": "Harmony", "tag": "I" }, "style" : {"color": "#ff7777"} },
      { "filter" : { "type": "Cadence"}, "style" : {"color": 'by-tag'} },
      { "filter" : { "type": "Pattern", "layers": ["predicted"] }, "style" : {"line": "top.2"} }
    ]
  }
}
```

Some examples: *(TODO, put LouisC examples with layout block)*
- [Fugue BWV 847](http://ws.dezrann.net/corpus/bach-fugues/bwv847/analyses/02-reference.dez)
- [Die Wetterfahne](http://ws.dezrann.net/corpus/schubert-winterreise/02/analyses/02.dez), from Schubert Winterreise Dataset


## `labels`: list of annotations

The `labels` list is the only mandatory part of a `.dez` file.
Each label may have the following attributes:

- `start` *(musical time, float, mandatory)*: This is the only mandatory field.
- `ioi` *(musical time, float, default 0)*: inter-offset-interval (IOI), that is duration from the start of the first note to the *start* of the last note
- `duration` *(musical time, float, default 0)*: actual duration, that is duration from the start of the first note to the *end* of the last note

A label with no `ioi` neither `duration` is supposed be "instant" (and will be displayed without any width).
The `ioi` may allow better comparison between perceptually similar labels, however
the `duration` is closer to what people usually mean by "duration".

A label can have both a `duration` and a `ioi`.
The conversion between the two is sometimes ambiguous, in particular in a polyphonic context (see dezrann#846).

- `staff` *(integer, default 0)*: staff concerned by the label, 1 being the staff on the top. If this is 0 or not defined, the label relates to all the score. *(TODO: allow name/id references also)*

- `type` *(string, default "")*: base type of the label, from a controled vocabulary. Dezrann displays some default types, but the default list could be configured for particular usages.

- `tag` *(string, default "")*: further details on the labels, depending on the type. This should again be somewhat controlled according to each analyst ontology/puropose.

- `comment` *(string, default "")*: comment, in free text. This can have several lines.

- `layers` *(string list, default "")*: annotation sources, such as `["tovey"]` or `["predicted, algorithm5"]`

Note that a label can also have graphic attributes (such as `line`), but this is generally not recommended:
One should use instead the `meta.layout` block as described below.

Thus the `labels` list should be as independent as possible from any actual implementation of Dezrann or any other visualisation platform.

Labels can be exported from `music21` with our `music21.schema` package. (2022: This has to be updated/replaced with the output of the RepresentingHarmony group).


### Musical time

`start`, `duration` and `ioi` are expressed in *quarter notes* from the beginning of the score.

In Dezrann, they are displayed, by default, in measure/beat/offset, the goal being to adhere as closely as possible
to "common musicological practice".
The meter information  in [`info.json`](infos-json.md) is currently too rigid:
`time-signature` (default 4/4), `time-signature-upbeat` (default 0).
The modelin of flexible meters is in discussion with collaborators and will be introduced in Q1 2023.

*TODO: Can be encoded as measure/beat/offset in .dez ?*



## `meta`: analysis and piece metadata

The `meta` block list metadata attributes of the analysis and possibly of the piece (#899).

## `meta.layout`: label formatting options

The `meta.layout` block lists rules defining styling options for the labels,
as in `{ "filter" : { "type": "Pedal" }, "style" : {"line": "top.1", "color": "#bbbbbb"} }`.

Each rule has

- a `filter` attribute, now supporting `type` and `layers` queries.
- a `"style"` attribute, defining styling and formatting options. The currently supported option is:
    * `line`: By default, out-staff labels are displayed on six lines: `top.1`, `top.2`, `top.3` `bot.1`, `bot.2`, `bot.3`
    * `color` will be supported dezrann#937
      The 'by-tag' special value indicates that a different color is applied to each tag.

# References

- M. Giraud, R. Groult, E. Leguy. [Dezrann, a Web Framework to Share Music Analysis](https://hal.archives-ouvertes.fr/hal-01796787). International Conference on Technologies for Music Notation and Representation (TENOR 2018), pp.104-110.

- L. Garczynski et al., [Modeling and editing cross-modal synchronization on a label web canvas](https://hal.archives-ouvertes.fr/hal-03583179), Music Encoding Conference (MEC 2022)



