
# Dezrann Documentation

[Dezrann](http://www.dezrann.net) is an open platform to hear, study, and annotate music on the web.
Many [people](contributors) contribute to Dezrann.
See the [home repository](https://gitlab.com/algomus.fr/dezrann/dezrann)
for more information.

## General Information
- [How to contribute to Dezrann](contribute)
- [How to add and maintain a new corpus](new-corpus)
- [Open science and Licenses](licenses)
- [Algomus and Dezrann Privacy Policy](http://algomus.fr/privacy/)

## Corpus Documentation

See <https://gitlab.com/algomus.fr/dezrann/dezrann-corpus>.

### Existing Public Corpora
- [Status of Public Dezrann Corpora](status)
- [Rebuilding Corpora](rebuild)

### Technical Corpus Documentation
- [metadata .json](metadata)
- [.dez format specification](dez-format)
- [quality](quality)
- [synchronization](synchro)

### Dezrann Local Installation
- See <https://gitlab.com/algomus.fr/dezrann/dezrann-front/-/blob/dev/docs/en/install.md>