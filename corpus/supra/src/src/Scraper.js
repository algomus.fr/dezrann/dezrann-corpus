const cheerio = require('cheerio');
const Puppeteer = require('puppeteer');

const HOMEPAGE_URL = 'https://supra.stanford.edu/';

class Scraper {
  constructor(options) {
    if (options.only) {
      this.only = [...options.only];
    }
  }

  browser;

  page;

  pieces = [];

  retries = 0;

  only;

  async init() {
    this.browser = await Puppeteer.launch();
    this.page = await this.browser.newPage();

    while (this.pieces.length <= 0 || this.retries > 10) {
      await this.getPieces();
    }
    if (this.pieces.length > 0) {
      this.validateParsedPiece();
    }
  }

  get parsedPieces() {
    return this.pieces;
  }

  async getPieces() {
    await this.page.goto(HOMEPAGE_URL)
    const markup = await this.page.content();

    let $ = cheerio.load(markup);

    const pieces = $('tr.data');

    if (pieces.length <= 0) {
      this.retries++;
      console.log('Retry n°', this.retries);
    } else {
      this.pieces = [...this.parsePieces($, pieces)];
      console.log(this.pieces.length, 'pieces parsed')
    }
  }

  parsePieces($, pieces) {
    const parsedPieces = [];
    for (let piece of pieces) {
      const infos = $(piece).children()

      if (infos.length > 0) {
        const info = {
          label: $(infos[1]).text(),
          title: $(infos[2]).text(),
          composer: $(infos[3]).text(),
          performer: $(infos[4]).text(),
          rollAndAudioScan: $(infos[5]).children('span').children('a').attr('href'),
          sw: $(infos[6]).children('span').children('a').attr('href'), // SearchWorks: Stanford Libraries card catalogue entry
          sdr: $(infos[7]).children('span').children('a').attr('href'), // Stanford Digital Repository entry
          rollLeader: $(infos[8]).children('span').children('a').attr('href'),
          imgQualityAnalysis: $(infos[9]).children('span').children('a').attr('href'),
          driftAnalysis: $(infos[10]).children('span').children('a').attr('href'),
          tif: $(infos[11]).children('span').children('a').attr('href'),
          midiExp: $(infos[12]).children('span').children('a').attr('href'),
          midiRaw: $(infos[13]).children('span').children('a').attr('href'),
          mp4: $(infos[14]).children('span').children('a').attr('href'),
          mp3: $(infos[15]).children('span').children('a').attr('href'),
          marcxml: `https://raw.githubusercontent.com/pianoroll/SUPRA/master${$(infos[1]).children('a').attr('href').split('/master')[1]}`,
        }
        if (
          !this.only.length ||
          this.only.includes(parseInt(info.label.split(' ')[1]))
        ) {
          parsedPieces.push(info);
        }
      }
    }

    return parsedPieces;
  }

  validateParsedPiece() {
    console.log(`===== Validating ${this.pieces.length} pieces =====`)
    const missing = [];
    for (let p of this.pieces) {
      if (!p.label) missing.push(p);
      if (!p.title) missing.push(p);
      if (!p.composer || p.composer === '???') missing.push(p);
      if (!p.performer) missing.push(p);
      if (!p.rollAndAudioScan) missing.push(p);
      if (!p.sw) missing.push(p);
      if (!p.sdr) missing.push(p);
      if (!p.rollLeader) missing.push(p);
      if (!p.imgQualityAnalysis) missing.push(p);
      if (!p.driftAnalysis) missing.push(p);
      if (!p.tif) missing.push(p);
      if (!p.midiExp) missing.push(p);
      if (!p.midiRaw) missing.push(p);
      if (!p.mp4) missing.push(p);
      if (!p.mp3) missing.push(p);
    }
    missing.map(m => {
      Object.keys(m).map(key => {
        if (m[key] === null | m[key] === '???')
          console.log(`Missing ${key} for piece [${m.label}] ${m.title}`);
      })
    })
  }
}

module.exports = Scraper;