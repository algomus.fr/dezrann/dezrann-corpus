# **Supra Scraper**

This script scrap pieces of the [SUPRA corpora](https://supra.stanford.edu/) and format them to be used with Dezrann.

It outputs all generated files with the right hierarchy to `./pieces/`

## **Getting started**

### **Installation**

``` bash
yarn
```

### **Launch**

``` bash
yarn start
```

<hr>

## **Options**

### **Help**

``` txt
yarn start -h

Options :
      --version  Affiche le numéro de version               [boolean]
      --only     Fetch only the given supra label number list
                 example: --only 10 26 40                   [array]
  -s, --skip     If the piece is already complete, skips it [boolean]
  -h, --help     Affiche l'aide                             [boolean]
```
