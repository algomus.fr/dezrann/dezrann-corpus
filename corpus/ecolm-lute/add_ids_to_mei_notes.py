# THIS SCRIPT ADDS IDs TO MEI NOTES
# It is required because the backend needs IDs to be able to identify notes
#
# This program must be used as follows:
#
#     python3 add_ids_to_mei_notes [-f] <input> [<output>]
#
# where :
# - `<input>`: input MEI filename
# - `<output>`: output MEI filename
#     - if not specified, modifications are made in place
# - if option `-f` is provided, `<input>` and `<output>` are folders and the modifications are made on all MEIs within the folder
#
# For example, to add IDs to \<notes/\> in all MEIs in a folder `mei/`, type:
#
#     python3 add_ids_to_mei_notes -f mei



import getopt
import random
import os
import string
import sys

import lxml.etree as ET


MEI_NAMESPACE = {'mei': 'http://www.music-encoding.org/ns/mei'}
XML_NAMESPACE = {'xml': 'http://www.w3.org/XML/1998/namespace'}


def get_xml_tree(xml_path: str) -> ET.ElementTree:
    """Returns the tree of the XML file"""
    return ET.parse(xml_path)

def _generate_unique_id(existing_ids):
    """Generates a unique NCName-compatible ID that is not in the list of existing IDs"""
    ID_SIZE = 30
    while True:
        # Generate a random string of 10 alphanumeric characters
        letter = random.choices(string.ascii_letters)[0]
        new_id = letter + ''.join(random.choices(string.ascii_letters + string.digits, k=ID_SIZE-1))
        if new_id not in existing_ids:
            return new_id

def add_ids_to_notes(xml_tree: ET.ElementTree):
    """Adds IDs to notes in the XML tree

    If a note already has an ID, it is not modified. If a note does not have an
    ID, one is generated and added. We make sure that the generated IDs are
    unique and cannot be equal to any existing ID in the XML tree.
    """
    root = xml_tree.getroot()
    notes = root.findall(".//mei:note", MEI_NAMESPACE)
    existing_ids = {note.get(f"{{{XML_NAMESPACE['xml']}}}id") for note in notes if f"{{{XML_NAMESPACE['xml']}}}id" in note.attrib}
    for note in notes:
        if f"{{{XML_NAMESPACE['xml']}}}id" not in note.attrib:
            unique_id = _generate_unique_id(existing_ids)
            note.set(f"{{{XML_NAMESPACE['xml']}}}id", unique_id)
            existing_ids.add(unique_id)

def write_xml_tree(xml_tree: ET.ElementTree, xml_path: str):
    """Writes the XML tree to a file"""
    xml_tree.write(xml_path, pretty_print=True, xml_declaration=True, encoding="UTF-8")


def main(argv):
    MEI_EXT = '.mei'
    folder_option = False

    try:
        opts, args = getopt.getopt(argv[1:], "f", ["folder"])
    except getopt.GetoptError:
        print(f'Usage: python3 {__file__} [-f] <input> [<output>]')
        sys.exit(1)

    if len(args) < 1 or len(args) > 2:
        print(f'Usage: python3 {__file__} [-f] <input> [<output>]')
        sys.exit(1)

    for opt, arg in opts:
        if opt in ("-f", "--folder"):
            folder_option = True
    
    input = args[0]                                    # `input`` and `output`` are either .mei filepaths
    output = args[1] if len(args) == 2 else input      # or folder paths

    if folder_option:
        mei_filenames = [filename for filename in os.listdir(input) if filename.endswith(MEI_EXT)]
        input_meis = [os.path.join(input, filename) for filename in mei_filenames]
        output_meis = [os.path.join(output, filename) for filename in mei_filenames]
        if not os.path.exists(output):
            os.makedirs(output)
    else:
        if not input.endswith(MEI_EXT) or not output.endswith(MEI_EXT):
            print(f'Error: arguments must be {MEI_EXT} files.')
            sys.exit(1)
        input_meis = [input]
        output_meis = [output]

    for i in range(len(input_meis)):
        xml_tree = get_xml_tree(input_meis[i])
        add_ids_to_notes(xml_tree)
        write_xml_tree(xml_tree, output_meis[i])
        
    

if __name__ == '__main__':
    main(sys.argv)
