corpusDir="../../../dezrann/corpus"

for dir in ./corpus/*/
do
    dir=${dir%*/}
    echo "${dir##*/}"
    curl --request POST \
    --url http://localhost:8000/addNewPiece/erkomaishvili/ \
    --header 'Content-Type: multipart/form-data' \
    --form score=@./corpus/${dir##*/}/sources/images/xml/${dir##*/}_text.xml \
    --form infos=@./corpus/${dir##*/}/piece.json \
    --form audio=@./corpus/${dir##*/}/sources/audios/audio/${dir##*/}_mix.mp3 \
    --form synchro=@./corpus/${dir##*/}/sources/audios/synchro/${dir##*/}_mix.json \
    --form audio=@./corpus/${dir##*/}/sources/audios/audio/${dir##*/}_S1.mp3 \
    --form synchro=@./corpus/${dir##*/}/sources/audios/synchro/${dir##*/}_Seg1.json \
    --form audio=@./corpus/${dir##*/}/sources/audios/audio/${dir##*/}_S2.mp3 \
    --form synchro=@./corpus/${dir##*/}/sources/audios/synchro/${dir##*/}_Seg2.json \
    --form audio=@./corpus/${dir##*/}/sources/audios/audio/${dir##*/}_S3.mp3 \
    --form synchro=@./corpus/${dir##*/}/sources/audios/synchro/${dir##*/}_Seg3.json
    # cp \
    #     ./corpus/${dir##*/}/sources/audios/synchro/synchro_mix.json \
    #     ${corpusDir}/sandbox/erkomaishvili/${dir##*/}_text/sources/audios/${dir##*/}_text-0/synchro.json


done
cp ./access.json ${corpusDir}/sandbox/erkomaishvili/
cp ./corpus.json ${corpusDir}/sandbox/erkomaishvili/