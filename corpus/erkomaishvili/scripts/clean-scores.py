

import glob
import xml.etree.ElementTree as ET

FILES = '../corpus/GCH_*/sources/images/xml/*.xml'

def clean_gch(tree):
    for part in tree.getroot().findall('part'):
        for measure in part.findall('measure'):
            for attributes in measure.findall('attributes'):
                for time in attributes.findall('time'):
                    attributes.remove(time)
            for note in measure.findall('note'):
                for child in note:
                    if 'name' in child.attrib:
                        if child.attrib['name'] == 'refnum':
                            note.remove(child)


for f in glob.glob(FILES):
    print(f)
    # ff = f.replace('_text', '').replace('xml/GCH_', 'xml/GCH-')

    tree = ET.parse(f)
    clean_gch(tree)

    with open(f, 'w') as fz:
        tree.write(fz, encoding='unicode')