import { Directory, File } from "../util/types";
import Util from "../util/FSUtil";

export default class DataSaver {
    private directories: string[];
    private instance: Util;

    constructor(private name: string) {
        this.instance = Util.getInstance();
    }

    saveData(data: Directory | File, index: number, write: boolean = false, specialWriting: boolean = false) {
        this.getDirectories();

        const newPath: string = this.instance.join(process.env.FINAL_PATH, this.directories[index], this.name);

        this.instance.createDirsFromPaths(false, newPath);
        if ("value" in data || write) this.writeData(newPath, data, specialWriting)
        else this.writeData(newPath, data.valueList[index], specialWriting);
        
    }

    private writeData(dest: string, source: File | Directory, specialWriting:boolean) {
        let newPath: string = this.instance.join(dest, source.path.split("/").pop());
        if ("value" in source) {
            if (specialWriting) this.instance.writeFileSync(newPath, source.value, "binary");
            else this.instance.writeFileSync(newPath, source.value);
        }
        else {
            this.instance.createDirsFromPaths(true, newPath);
            for (let file of source.valueList) {
                this.writeData(newPath, file, specialWriting);
            }
        }
    }

    private getDirectories() {
        this.directories = this.instance.readdirSync(process.env.FINAL_PATH);
    }
}