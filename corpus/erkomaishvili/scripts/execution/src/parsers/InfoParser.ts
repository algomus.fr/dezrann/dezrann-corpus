import { Directory, File } from "../util/types";
import Util from "../util/FSUtil";



export default class InfoParser {
    private instance: Util;
    private jsonData: {key: string, value: string}[];
    private id: string;
    infosObject = {}

    constructor() {
        this.instance = Util.getInstance();
    }

    parseData(infos: Directory): Directory {

        for (let info of infos.valueList) {
            this.jsonData = JSON.parse((info as File).value as string);
            this.id = info.path.split("/").pop();
            this.parseInfo(info as File);
            this.rename(info as File);
            const infoObject = this.info();
            this.infosObject[infoObject.opus.id] = infoObject;
        }

        return infos;
    }

    private rename(info: File) {
        info.path = this.instance.join(this.instance.splitDirPathAndLastPath(info.path)[0], "piece.json");
    }

    private parseInfo(info: File) {
        info.value = JSON.stringify(this.info, null, "\t");
    }

    private info() {
        const res: Record<string, any> = {};
        res["opus"] = this.opus();
        res["sources"] = this.sources();
        res["time-signature"] = "1/4";
        res["quality:musical-time"] = "3";
        res["quality:score"] = "4";
        res["quality:audio"] = "5";
        res["quality:audio:synchro"] = "4";
        return res
    }

    private opus(): Record<string, any> {
        const res: Record<string, any> = {};
        res["id"] = this.id.replace('_', '-');
        res["opus"] = this.id.replace('_', '-');
        res["contributors"] = this.contributors();
        res["title"] = this.getValue("Georgian Title");
        res["title:en"] = this.getValue("English Title");
        res["corpus"] = "Erkomaishvili";
        res["year"] = this.getValue("Year");
        res["ref"] = this.instance.join(process.env.PAGE_TAB, `${this.id}_Erkomaishvili`);
        res["ref:publication"] = "https://doi.org/10.5334/tismir.44";

        return res;
    }

    private contributors(): Record<string, any> {
        const res: Record<string, any> = {};
        const transcriberTable: string[] = this.getValue("Score").split(" ");
        const tmp = transcriberTable.pop();
        const transcriber: string = transcriberTable.pop() + " " + tmp;
        res["performer"] = this.getValue("Singer");
        res["transcriber"] = transcriber;

        return res;
    }

    private putMixFirstInList(list: any[]) {
        const mix = list.pop()
        return list.splice(0, 0, mix);
    }

    private sources(): Record<string, any> {
        const list: Record<string, any>[] = [];
        const piecePath = '../../../corpus/erkomaishvili/corpus/' + this.id + '/';
        const xml: File = this.instance.getFiles(this.instance.join(process.env.FINAL_PATH, this.id, process.env.XML_DIR_NAME)).valueList[0] as File;
        list.push({"score": piecePath + this.instance.join(process.env.XML_DIR_NAME, this.instance.splitDirPathAndLastPath(xml.path)[1])});

        const audios: Directory = this.instance.getFiles(this.instance.join(process.env.FINAL_PATH, this.id, process.env.AUDIO_DIR_NAME));
        this.putMixFirstInList(audios.valueList);
        const synchros: Directory = this.instance.getFiles(this.instance.join(process.env.FINAL_PATH, this.id, process.env.SYNCHRO_DIR_NAME));
        this.putMixFirstInList(synchros.valueList);
        for (let index=0; index < audios.valueList.length; index++) {
            list.push(
                this.source(
                    this.instance.join(process.env.AUDIO_DIR_NAME, this.instance.splitDirPathAndLastPath(audios.valueList[index].path)[1]),
                    this.instance.join(process.env.SYNCHRO_DIR_NAME, this.instance.splitDirPathAndLastPath(synchros.valueList[index].path)[1]),
                    piecePath
                )
            );
        }

        return list;
    }

    private source(audio: string, synchro: string, piecePath: string): Record<string, any> {
        const res: Record<string, any> = {};
        res["audio"] = piecePath + audio;
        res["synchro"] = piecePath + synchro;
        res["license"] = "CC-BY-NC-4.0";

        return res;
    }

    private getValue = (theKey: string) => this.jsonData.filter(data => data.key == theKey)[0].value;
}
