import { Directory, DivCount, File, JSONData } from '../util/types';
import Util from "../util/FSUtil";

// The class parsing raw csv files into synchro.json files
export default class OnSetParser {
    private instance: Util;

    // It will gather all the data found and parsed during the process
    private generalDir: Directory = {
        path: "",
        valueList: []
    };

    // Initializes this.generalDir with outputPath, and dirName
    constructor(private name: string, private nb_gathered: number) {
        this.instance = Util.getInstance();

        this.generalDir.path = this.instance.join(process.env.RESOURCES_PATH, this.name);
    }

    // Runs all the process to get the parsed synchro.json files
    parseData(dir: Directory): Directory {
        this.manageSynchroFiles(dir.valueList, this.nb_gathered);

        this.parseOnsetAnnotations();

        return this.generalDir;
    }

    // Creates all the structure in comparison to the raw data
    private manageSynchroFiles(files: (File | Directory)[], gather: number = 1): void {
        if (gather <= 0) {
            console.error("Cannot gather 0 or less file together");
            return;
        }
        if (files.length <= 0) {
            console.error("No files to parse");
            return;
        }

        let currentIndex: number = 1;
        let tmpSynchroDir: Directory = {
            path: this.instance.join(this.generalDir.path, this.name),
            valueList: []
        };
        for (let file of files) {

            // Pushing the SynchroFile in his planned directory
            tmpSynchroDir.valueList.push(file);

            // If it pushed enough SynchroFile in the current directory
            if (currentIndex%gather == 0) { 

                // Pushing it in the general directory and notify the terminal
                this.generalDir.valueList.push(tmpSynchroDir);

                // Creating new directory for others SynchroFiles
                tmpSynchroDir = {
                    path: this.instance.join(this.generalDir.path, this.name),
                    valueList: []
                };
            }
            currentIndex++;
        }

        // If there is not enough files (assuming for now that there is
        // an equal number of files for each directory)
        if (currentIndex%gather != 1) {
            console.error("Files are missing");
            return;
        }
    }

    // Gets the interesting data in each csv file and transform it 
    // into a synchro.json file
    private parseOnsetAnnotations(): void {
        for (let synchroDir of this.generalDir.valueList) {
            let dataList: JSONData[][] = [];
            let id: string = "";
            for (let index = 0; index < (synchroDir as Directory).valueList.length; index++) {
                let file: File = (synchroDir as Directory).valueList[index] as File;
                let parsed: JSONData[] = this.generateSynchro(file);

                id = this.instance.removeLast(file.path, "_");

                dataList.push(parsed);
            }
            // Needed for this dataset because the synchro doesn't exist
            // for the whole piece in each piece
            if (this.nb_gathered > 1) this.createMixSynchro(dataList, synchroDir as Directory, id);
        }
    }

    // Reads csv file and write converted file into json data
    private generateSynchro(synchroFile: File): JSONData[] {

        let data: string | Buffer = synchroFile.value;

        // Transforming data
        const parsedData: JSONData[] = this.parseCSVtoJSON(data);

        synchroFile.path = synchroFile.path.split('.csv').shift() + ".json";
        synchroFile.value = JSON.stringify(parsedData, null, '\t');

        return parsedData; // Needed to create the mixed synchro file
    }

    // Transforms the csv file into a json data
    private parseCSVtoJSON(data: string | Buffer) {
        const parsedData: JSONData[] = [];

        const lines: string[] = (data.toString()).split('\n');

        for (let line of lines.slice(0, lines.length - 1)) { // Removing last empty line
            const lineList:string[] = line.split(',');
            const data: JSONData = {
                onset: parseFloat(lineList[3]) -1,
                date: parseFloat(parseFloat(lineList[1]).toFixed(2)) 
                // Fixing numbers after coma, because there are too much
                // in raw files
            }
            parsedData.push(data);
        }

        return parsedData;
    }

    // Creates the synchro.json needed for the whole piece's audio
    private createMixSynchro(dataList: JSONData[][], synchroDir : Directory, id: string) {
        const results: [File, JSONData[]] = this.mergeSynchroFiles(dataList, synchroDir, id);

        const newFile: File = results[0];
        const result: JSONData[] = results[1];

        // Final step: writing it in the value field of the newly created File
        // and pushing it in synchroDir
        newFile.value = JSON.stringify(result, null, '\t');
        synchroDir.valueList.push(newFile);
    }

    private mergeSynchroFiles(dataList: JSONData[][], synchroDir: Directory, id: string): [File, JSONData[]] {
        // Taking the last list of the dataList and 
        // deleting it from dataList, also transforming it into DivCount data
        // This is more practical when working with 
        // mean computation, because numbers are summed one after the other
        let data: DivCount[] = (dataList.pop() as JSONData[]).map(el => {
            return {json: el, div: 1};
        });

        // Creating the new SynchroFile to push at the end
        let newFile: File = {
            path: this.instance.join(synchroDir.path, `${id}_mix.json`),
            value: ""
        };

        // For each element in each list, if it does not exist in the
        // current data, simply adding it, else computing the mean
        for (let list of dataList) {
            for (let element of list) {
                let sameOnSet: DivCount[] = data.filter(el => el.json.onset == element.onset)
                if (sameOnSet.length == 0) {
                    data.push({json: element, div: 1});
                } else if (sameOnSet.length == 1) {
                    let same: DivCount = sameOnSet[0];
                    same.json.date = parseFloat((((same.json.date * same.div) + element.date) / (same.div + 1)).toFixed(2));
                    same.div++;
                } else {
                    console.error("Too much data with the same onset !")
                }
            }
        }

        // Sorting it to have it in the planned order
        data.sort((a, b) => {
            return a.json.onset - b.json.onset;
        })

        // Returning to the planned synchro.json format
        let result: JSONData[] = data.map(el => el.json);

        return [newFile, result];
    }
}