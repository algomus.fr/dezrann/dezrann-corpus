import ZipGetter from "./getters/ZipGetter";
import { Directory } from "./util/types";
import RepositoryMaker from "./savers/RepositoryMaker";
import DataSaver from "./savers/DataSaver";
import FSUtil from "./util/FSUtil";

export default class XMLCollector {
    private XMLList: Directory | null;
    private getter: ZipGetter;
    private IDList: string[] = [];
    private maker: RepositoryMaker;
    private saver: DataSaver;
    private instance: FSUtil;

    constructor(private download: boolean, private exec: boolean) {
        this.instance = FSUtil.getInstance();

    	this.maker = new RepositoryMaker();
        this.getter = new ZipGetter(process.env.XML_NAME)

        this.saver = new DataSaver(process.env.XML_DIR_NAME);
    }

    collectXML() {
        if (this.download) {
            this.getXML();
        } else {
            this.readXML();
        }

        this.getIDs();
        this.writeIDs();

        if (this.exec && this.XMLList) {

            this.makeRepo();

            this.saveXML();
        }
    }

    private getXML() {
        process.stdout.write("Getting XML source... ");
        this.XMLList = this.getter.getData(process.env.XML_SOURCE_PATH, true);
        console.log("OK");
    }
    private readXML() {
        process.stdout.write("Reading XML resources... ");
        if (this.instance.existsSync(this.instance.join(process.env.RESOURCES_PATH, process.env.XML_NAME)) === false) {
            this.XMLList = null;
            console.log("ERROR : no such file or directory");
            return;
        }
        this.XMLList = this.instance.getFiles(this.instance.join(process.env.RESOURCES_PATH, process.env.XML_NAME));
        console.log("OK");
    }

    private getIDs() {
        for (let xml of this.XMLList.valueList) {
            this.IDList.push(xml.path.split("/").pop().split("_text").shift());
        }
    }

    private writeIDs() {
        this.instance.writeFileSync(this.instance.join(process.env.RESOURCES_PATH, process.env.ID_FILE), JSON.stringify(this.IDList));
    }

    private makeRepo() {
        process.stdout.write("Creating final repository... ");
        this.maker.makeRepo(this.IDList);
        console.log("OK");
    }

    private saveXML() {
        process.stdout.write("Saving XML files... ");
        for (let i = 0; i < this.XMLList.valueList.length; i++)
            this.saver.saveData(this.XMLList, i);
        console.log("OK");
    }
}
