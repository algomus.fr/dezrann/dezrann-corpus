import TabGetter from "./getters/TabGetter";
import InfoParser from "./parsers/InfoParser";
import DataSaver from "./savers/DataSaver";
import { Directory, File } from "./util/types";
import Util from "./util/FSUtil";
import { writeFileSync } from "fs";

export default class InfoCollector {
    private instance: Util;

    private getter: TabGetter;
    private parser: InfoParser;
    private saver: DataSaver;
    private infoList: Directory | null;
    private parsedList: Directory;
    private IDList: string[];

    constructor(private download: boolean, private exec: boolean) {
        this.instance = Util.getInstance();

        this.getter = new TabGetter(process.env.INFO_SOURCE_PATH, process.env.INFO_NAME, [
            {
                parent: "table.table",
                path: "td strong",
                attribute: "text"
            },
            {
                parent: "table.table",
                path: "td:nth-child(2)",
                attribute: "text"
            }
        ]);

        this.parser = new InfoParser();

        this.saver = new DataSaver(process.env.INFO_DIR_NAME);

        this.infoList = {
            path: process.env.INFO_DIR_NAME,
            valueList: []
        }
        this.IDList = JSON.parse(this.instance.readFileSync(this.instance.join(process.env.RESOURCES_PATH, process.env.ID_FILE)).toString());
    }

    async collectInfo() {

        if (this.download) await this.getInfoTab();
        else this.readInfoTab();

        if (this.exec && this.infoList) {
            this.parseInfo();

            this.saveInfo();
        }
    }

    private async getInfoTab() {
        console.log("** Getting info source **");
        for (let id of this.IDList) {
            process.stdout.write(`Getting from ${id}... `);
            let info: File = await this.getter.getData(`${id}_Erkomaishvili`);
            this.infoList.valueList.push(info);
            console.log("Done");
        }
        console.log("OK");
    }

    private readInfoTab() {
        process.stdout.write("Reading info resources... ");
        if (this.instance.existsSync(this.instance.join(process.env.RESOURCES_PATH, process.env.INFO_NAME)) === false) {
            this.infoList = null;
            console.log("ERROR : no such file or directory");
            return;
        }
        this.infoList = this.instance.getFiles(this.instance.join(process.env.RESOURCES_PATH, process.env.INFO_NAME));
        console.log("OK");
    }

    private parseInfo() {
        process.stdout.write("Parsing info... ");
        this.parsedList = this.parser.parseData(this.infoList);
        console.log("OK");
    }

    private saveInfo() {
        process.stdout.write("Saving info... ");
        writeFileSync('../corpus/erkomaishvili-pieces.json', JSON.stringify({ pieces: this.parser.infosObject}, null, "\t"));
        console.log("OK");
    }
}
