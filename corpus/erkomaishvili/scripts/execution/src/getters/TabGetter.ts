import { File } from "../util/types";
import Util from "../util/FSUtil";
import PUtil from "../util/PuppetUtil";

export default class TabGetter {
    newDirPath: string;
    instance: Util;
    PInstance: PUtil;

    constructor(private source: string, private name: string, private attributesToGet: {parent: string, path: string, attribute:string}[]){
        this.instance = Util.getInstance();
        this.PInstance = PUtil.getInstance();
        this.newDirPath = this.instance.join(process.env.RESOURCES_PATH, this.name);
    }

    async getData(id: string): Promise<File> {
        this.createResourcesDirectory();

        let res: File = {
            path: this.instance.join(this.newDirPath, this.instance.removeLast(id, "_")),
            value: ""
        };

        let data: {key: string, value: string}[] = await this.extractFromLink(this.instance.join(this.source, id));
        res.value = JSON.stringify(data, null, '\t');
        this.instance.writeFileSync(res.path, res.value);

        return res;
    }

    private async extractFromLink(link: string): Promise<{key: string, value: string}[]> {
        await this.PInstance.goto(link);

        let tab: string[][] = await this.PInstance.getTagsContent(...(this.attributesToGet));
        let [keys, values]: [string[], string[]] = [tab[0], tab[1]];
        let res: {key: string, value: string}[] = [];
        for (let index in keys) {
            res.push({
                key: keys[index],
                value: values[index]
            })
        }

        return res;
    }

    private createResourcesDirectory() {
        this.instance.createDirsFromPaths(false, this.newDirPath);
    }
}