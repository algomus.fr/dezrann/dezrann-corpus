import * as fs from 'fs';
import path from 'path';
import * as rimraf from "rimraf";
import { Directory, File } from "./types";

export default class FSUtil {
    private static instance: FSUtil;

    private constructor(){}

    static getInstance(): FSUtil {
        if (!FSUtil.instance) FSUtil.instance = new FSUtil();

        return FSUtil.instance;
    }

    splitDirPathAndLastPath(path: string): [string, string] {
        const tmp: string[] = path.split("/");
        let last: string = tmp.pop() as string;
        if (last == "") last = tmp.pop() as string;
        return [tmp.join("/"), last];
    }

    createDirsFromPaths(remove: Boolean, ...paths: string[]) {
        for (let path of paths) {
            this.createDirsFromPath(remove, path);
        }
    }
    private createDirsFromPath(remove: Boolean, outputPath: string) {
        let eachPath: string[] = outputPath.split("/");
        if (eachPath[0] == '.') eachPath.shift();
        if (eachPath.length > 1) { 
            eachPath.pop();
            let currentPath: string = "";

            for (let pathPart of eachPath) {
                currentPath = path.join(currentPath, pathPart);
                if (fs.existsSync(currentPath)) continue;
                fs.mkdirSync(currentPath);
            }
        }
        if(remove) {
            if (fs.existsSync(outputPath)) rimraf.sync(outputPath);
        }
        if (!fs.existsSync(outputPath)) fs.mkdirSync(outputPath);
    }

    removeDirsFromPaths(...paths: string[]) {
        for (let path of paths) {
            if(fs.existsSync(path)) rimraf.sync(path);
        }
    }

    getFiles(sourcePath: string): Directory {
        const extracted : string[] = this.readdirSync(sourcePath);
        const files: Directory = {
            path: sourcePath,
            valueList: []
        }
        for (let element of extracted) {
            const tmp = element.split("_");
            const first: string = tmp.shift();
            let id: string = `${first}_${tmp.shift()}`;
            const last: string = tmp.pop();
            if (last && (!last.includes("Erkomaishvili"))) id = `${id}_${last}`;
            let fullPath: string = this.join(sourcePath, element);
            const stat = this.statSync(fullPath);
            if (stat.isDirectory()) 
                files.valueList.push(this.getFiles(fullPath));
            else if(stat.isFile())
                files.valueList.push({
                    path: this.join(sourcePath, id),
                    value: this.readFileSync(fullPath)
                    });
        }
        return files;
    }

    removeLast(path: string, separator: string): string {
        let tmp = path.split(separator);
        if (tmp.pop() == "") tmp.pop();
        return tmp.join(separator);
    }

    join = path.join;
    writeFileSync = fs.writeFileSync;
    readFileSync = fs.readFileSync;
    readdirSync = fs.readdirSync;
    mkdirSync = fs.mkdirSync;
    renameSync = fs.renameSync;
    statSync = fs.statSync;
    existsSync = fs.existsSync;
}