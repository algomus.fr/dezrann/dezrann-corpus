import XMLCollector from './src/XMLCollector';
import SynchroCollector from './src/SynchroCollector';
import AudioCollector from './src/AudioCollector';
import InfoCollector from './src/InfoCollector';
import { config } from 'dotenv';
import PuppetUtil from "./src/util/PuppetUtil";
import yargs from "yargs";

class Index {
    private download: boolean;
    private exec: boolean;

    constructor() {}

    async main() {
        let argv = yargs.options({
            exec: {
                alias: 'e',
                description: 'Parses resources from the "resources" folder, to put them in final folder',
                type: 'boolean',
                default: false
            },
            download: {
                alias: 'd',
                description: 'Downloads resources given by some sources and places them in "resources" folder',
                type: 'boolean',
                default: false
            },
            filter: {
                alias: 'f',
                description : 'Will tell to the script to make actions only for those resources',
                type: 'array',
                default: ["xml", "synchro", "audio", "info"]
            }
        }).argv;

        this.download = !argv["exec"] || argv["download"];
        this.exec = !argv["download"] || argv["exec"];

        config({ path: "./config.env" });
        await PuppetUtil.getInstance().initPage();

        if (argv["filter"].includes("xml")) {
            console.log("+++ XML Collector +++");
            new XMLCollector(this.download, this.exec).collectXML();
        }
        if (argv["filter"].includes("synchro")) {
            console.log("+++ Synchro Collector +++");
            new SynchroCollector(this.download, this.exec).collectSynchro();
        }
        if (argv["filter"].includes("audio")) {
            console.log("+++ Audio Collector +++");
            await new AudioCollector(this.download, this.exec).collectAudio();
        }
        if (argv["filter"].includes("info")) {
            console.log("+++ Info Collector +++");
            await new InfoCollector(this.download, this.exec).collectInfo();
        }

        PuppetUtil.getInstance().closePage();
    }
}
new Index().main();