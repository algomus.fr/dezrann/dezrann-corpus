
# Converting WJDB to Dezrann format

Author : Quentin DINEL

## Packages to install (not included in basic Python library)

```py
# Ubuntu
libsqlite3-dev

# Pip
pandas # dataframes
pysqlite3 # database
alive_progress # progress bar
music21
```

``` bash
sudo apt install libsqlite3-dev

pip install pandas pysqlite3 alive_progress music21
```

## How to use

### JAZZ

Be in the jazz folder and use these commands in a terminal :  

```console
wget -N -P ./data https://jazzomat.hfm-weimar.de/download/downloads/wjazzd.db
```

-> Stores the Weimar Jazz Database into the data folder (if not already there nor updated);

```console
wget -N -P ./data http://mir.audiolabs.uni-erlangen.de/jazztube/static/csv_youtube.csv
```

-> Stores the Youtube IDs' table into the data folder (if not already there nor updated);

-> clone https://gitlab.com/algomus.fr/jazz-schubert.git at the same level of dezrann-corpus (sibling directory)

```console
python3 code/convert_wjdb.py data/wjazzd.db data/csv_youtube.csv ../../../../jazz-schubert/jazz/data/wjd-musicxml/
```

- convert_wjdb.py :  
Python code executing all the code and generating a `songs` folder located in the parent folder `jazz`.
- wjazzd.db :  
The Weimar Jazz Database
- csv_youtube.csv :  
CSV file used to fetch Youtube ID of musical pieces' videos (Not every piece has a Youtube ID).  
- wjd-musicxml/ :  
XML files of every musical pieces.
