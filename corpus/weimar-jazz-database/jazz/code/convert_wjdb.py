import sys, os, pandas as pd, sqlite3, json,  shutil
from datetime import date
from alive_progress import alive_bar
from writing import WritingClass, OUT
from pymeasuremap.extract import extract_directory
import subprocess

SONGS_JSON = '../../../metadata/weimar-jazz-songs.json'

class ConvertWJDB():
    def __init__(self, _db, _yt, _xml):
        self.writing = WritingClass()

        self.db = _db
        self.yt = _yt
        self.xml_file = _xml
        self.DEZRANN_TYPES ={
            'CHORUS': 'Chorus',
            'FORM': 'Structure',
            'PHRASE': 'Phrase',
            'CHORD': 'Harmony',
            'IDEA': 'Pattern',
        }
        # get tables into panda dataframe
        [self.df_beats, self.df_sections, self.df_soloinfo, self.df_transcription, self.df_melody, self.df_trackinfo] = \
        self.getFromTables(["beats", "sections", "solo_info", "transcription_info", "melody", "track_info"])
        self.metadata = {
            'pieces': {}
        }
        
    def getFromTables(self, tables) :
        res=[]
        for table in tables :
            query = self.db.execute("SELECT * From " + table)
            cols = [column[0] for column in query.description]
            res.append(pd.DataFrame.from_records(data = query.fetchall(), columns = cols))
        return res

        

    def synchro_element(self, onset, bar, beat):
        return {"date":round(onset,2),"onset":(bar-1)*4+beat-1}

    def generate_synchro_dict(self, melid):
        res = []
        #solo_start = self.df_transcription[self.df_transcription['melid'] == melid]['solostart_sec']
        try:
            solo_start = self.yt[self.yt['melid'] == melid]['solo_start_sec'].values[0]
        except IndexError:
            #print("No solo start found in YT csv for melid : ", melid)
            solo_start = 0
        mel_beats = self.df_beats[self.df_beats['melid'] == melid]
        for i in range(len(mel_beats)):
            onset = mel_beats['onset'].values[i]
            bar = mel_beats['bar'].values[i]
            beat = mel_beats['beat'].values[i]
            res.append(self.synchro_element(float(onset) + float(solo_start), int(bar), int(beat)))
        if solo_start != 0:
            return res
        else:
            return "invalid"


    def end_start_time(self, bar, signature, beat):
        return int((bar-1)*signature + beat-1)

    def get_piece_info(self, melid):
        try:
            #print("melid : ", melid)
            solo_infos = self.df_soloinfo.loc[self.df_soloinfo['melid'] == melid]
            sections = self.df_sections.loc[self.df_sections['melid'] == melid]
            melodies = self.df_melody.loc[self.df_melody['melid'] == melid]
            # beats = self.df_beats.loc[self.df_beats['melid'] == melid]
            #remove rows (bar = 0) before real start (bar = 1, beat = 1)
            #beats = beats[beats['bar'] != 0]
            #Test : keeping difference between first db bar and bar 1 ("real start")
            diff = max(0, 1 - melodies.iloc[0]['bar'])
            labels = []
            factor = 1.0
            #print(len(melodies))
            upbeat = 0

            beatFirst = melodies.iloc[0]['beat'] - 1
            if beatFirst > 0 :
                upbeat = melodies.iloc[0]['period'] - beatFirst

            for i in range(len(sections)):
                section_type = sections.iloc[i]['type']
                section_start = sections.iloc[i]['start']
                section_end = sections.iloc[i]['end']
                value = sections.iloc[i]['value']
                if section_type in list(self.DEZRANN_TYPES.keys()):
                    section_type = self.DEZRANN_TYPES[section_type]

                barStart = melodies.iloc[section_start]['bar'] + diff
                signatureStart = melodies.iloc[section_start]['period']
                beatStart = melodies.iloc[section_start]['beat']
                barEnd = melodies.iloc[min(section_end + 1, len(melodies) - 1)]['bar'] + diff
                signatureEnd = melodies.iloc[min(section_end + 1, len(melodies) - 1)]['period']
                beatEnd = melodies.iloc[min(section_end + 1, len(melodies) - 1)]['beat']

                start = self.end_start_time(barStart, signatureStart, beatStart)
                end = self.end_start_time(barEnd, signatureEnd, beatEnd)

                duration = end - start
                # print(f"type: {section_type}, start: {start}, duration: {duration}, tag : {value}")
                label = {"type": section_type, "start": start * factor, "duration": duration * factor, "tag" : value}
                labels.append(label)

                # if melid == 41:
                #     print("---------------------------------")
                #     print("section_type :", section_type)
                #     print("value", value)
                #     print("barStart :", barStart)
                #     print("signatureStart :", signatureStart)
                #     print("beatStart :", beatStart)
                #     print("barEnd :", barEnd)
                #     print("signatureEnd :", signatureEnd)
                #     print("beatEnd :", beatEnd)
                #     print("start :", start)
                #     print("end :", end)
                #     print("duration :", duration)
                #     print("section_start :", section_start)
                #     print("section_end :", section_end)
                #     print("factor :", factor)
                #     print("eventid end :", melodies.iloc[min(section_end + 1, len(melodies) - 1)]['eventid'])
                #     print("---------------------------------")

            meta_date = str(date.today())
            meta_producer = "Dezrann"
            meta_title = solo_infos['title'].values[0]
            return labels, meta_date, meta_producer, meta_title, upbeat
        except (IndexError, UnboundLocalError) as e:
            print("Erreur sur ", melid, e)
            return {},'','',''




    

    def generate_corpus(self, performer):
        performer_info = self.df_soloinfo[self.df_soloinfo['performer'] == performer]
        performer_mels = performer_info['melid']
        for melid in performer_mels:
            labels, meta_date, meta_producer, meta_title, upbeat = self.get_piece_info(melid)
            self.writing.write_to_file(self.writing.create_dez_file(labels, meta_date, meta_producer, meta_title), "-d", performer, melid)
            #print(self.generate_synchro_dict(melid))
            if self.generate_synchro_dict(melid) != "invalid":
                self.writing.write_to_file(self.generate_synchro_dict(melid), "-s", performer, melid)
            performer_tracks = performer_info[performer_info['melid']==melid]['trackid']
            for trackid in performer_tracks:
                self.metadata['pieces'][f'{melid:04}'] = self.writing.get_metadata(melid, trackid, self.df_soloinfo, self.df_trackinfo, self.yt, upbeat)
            if len(performer_tracks) > 1:
                print('!', melid, ': several performer tracks')
    
    def generate_all_files(self, limit=None):
        performers = self.df_soloinfo["performer"].unique()
        if not os.path.isdir(OUT):
            os.mkdir(OUT)
        print("Generating corpus...")
        if not limit:
            limit = len(performers)
        with alive_bar(limit) as bar:
            for performer in performers[:limit]:
                self.generate_corpus(performer)
                bar.text = f'🎵 {performer}'
                bar()

    def write_songs_json(self):
        print('==>', SONGS_JSON)
        with open(SONGS_JSON, 'w') as f:
            json.dump(self.metadata, f, indent=2, sort_keys=True)

        print("Corpus generated.")
    



    def seperate_words(self, s):
        res = []
        for i in range(len(s)):
            if s[i].isupper() and i != 0:
                res.append(f' {s[i]}')
            else:
                res.append(s[i])
        return "".join(res)
    
    def xml_cleanup(self, xml_file_name):
        print(f'Clean up score {xml_file_name}')
        com = ["musescore.mscore", xml_file_name, "-o", xml_file_name]
        subprocess.run(com)
        print('score cleaned up.')

    def extractMmap(self, dir):
        print(f'Extractiong measure maps form xml in {dir}')
        try:
            extract_directory(f"{dir}", None, "*.xml")
            print("Measure maps extracted.")
        except:
            print(f'Error extracting mmap from score in {dir}')

    def copy_scores(self):
        melid_lower_titles_dict = {x : ["".join(self.df_soloinfo[self.df_soloinfo['melid'] == x]['performer'].values[0].lower().split()), "".join(self.df_soloinfo[self.df_soloinfo['melid'] == x]['title'].values[0].lower().split())] for x in self.df_soloinfo['melid']}
        print("Copying scores...", self.xml_file)
        for root, dirs, files in os.walk(self.xml_file):
            if not(files):
                print('! No files')
                return
            with alive_bar(len(files)) as bar:
                for file in files:
                    bar.text = f'🎵 {file}'
                    author = file.split('_')[0]
                    tail = file.split('_')[1]
                    #Check if song name contains '.', 9 corresponds to ".musicxml"
                    if len(tail.split('.')) > 2:
                        song = tail[0:len(tail) - 9]
                    else:
                        song = file.split('_')[1].split('.')[0]
                    #Checks if song contains 2 parts : "stardust-1", "stardust-2"
                    if '-' in song:
                        song = song.split('-')[0]
                    #Specific case when there's a '-' in the title, a "=" is used to differentiate with different song parts
                    if '=' in song:
                        song = song.split('=')[0] + '-' + song.split('=')[1]
                    try:
                        author_dir = self.seperate_words(author)
                        melid = list(melid_lower_titles_dict.keys())[list(melid_lower_titles_dict.values()).index([author.lower(), song.lower()])]   
                        if not f'{melid:04}' in self.metadata['pieces']:
                            print('! skip', melid)
                            continue
                        f = f"{self.xml_file}/{file}"
                        ff = f"{OUT}/{melid:04}/{melid:04}.xml"
                        self.xml_cleanup(ff)
                        self.extractMmap(f"{OUT}/{melid:04}/")
                        # print(f, '>', ff)
                        shutil.copy(f, ff)
                    except ValueError:
                        print(f"!! Performer and/or title mismatch: {author} {song} {file}")
                    bar()
        print("Scores copied.")
        


    def convert(self):
        self.generate_all_files()
        self.copy_scores()
        self.write_songs_json()


def main():
    db = sqlite3.connect(sys.argv[1])
    yt = pd.read_csv(sys.argv[2], encoding='utf-8')
    xml_file = sys.argv[3]
    cw = ConvertWJDB(db, yt, xml_file)
    cw.convert()

if __name__ == "__main__":
    main()
