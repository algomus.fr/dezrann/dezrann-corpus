
[Dezrann](http://www.dezrann.net) is an open platform to hear, study, and annotate music on the web.
See the [home repository](https://gitlab.com/algomus.fr/dezrann/dezrann)
for more information on Dezrann.

# Adding and maintaining corpora for Dezrann

This repository host metadata of the new corpora that will be published through Dezrann,
as well as [documentation](doc/), accessible from <https://doc.dezrann.net/>.
We are now in the process of better highlighting these corpora, and work with collaborators towards
a common publication in 2025 highlighting some corpora.

# FAQ

## Can I get the full list of corpora/piece currently on Dezrann?

As explained above, the individual `*.json` files for each corpus, in [metadata/](https://gitlab.com/algomus.fr/dezrann/dezrann-corpus/-/blob/main/metadata) give more information on the way each corpus is built.

The API call <https://ws.dezrann.net/corpus/recursive> gets the list of pieces you have access through <http://www.dezrann.net/corpora>. If you are logged, you may have access to more pieces than the public ones.

# Credits, reference

Dezrann is open-source (GPL v3+) and is developped by 
Emmanuel Leguy, Mathieu Giraud, Lou Garczynski, Charles Ballester, 
with the help of many [contributors](https://gitlab.com/algomus.fr/dezrann/dezrann/-/blob/dev/codemeta.json?ref_type=heads#L82) 
in the Algomus computer music team and elsewhere.