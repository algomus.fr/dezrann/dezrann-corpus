import tools

class MozartPianoSonatas(JsonCorpus):

    def parse_keys(self, filename):
        # input may be k279.1, k279-1.mscz, k279-1.musicxml, k279-1.mei
        key = filename.replace('K', 'k').replace('.mscz', '').replace('.musicxml', '').replace('.mei', '')
        key = key.replace('-', '.')
        assert(len(key) == 6)

        key_piece = key.split('.')[0]
        assert(len(key_piece) == 4)
        koechel = key_piece[1:]

        # k279 => K.279
        opus = "K." + koechel
        mvt = key.split('.')[1]

        return {
            KEY: key,
            KEYS: [key, key_piece, TEMPLATE],
            'opus': opus,
            'koechel': koechel,
            'mvt': mvt,
            'mvt_roman': tools.roman(int(mvt)).upper(), 
        }

    SONATAS = [
        'K279', 'K280', 'K281', 'K282', 'K283', 'K284',
        'K309', 'K310', 'K311', 'K330', 'K331', 'K332',
        'K333', 'K457', 'K533', 'K545', 'K570', 'K576',
    ]

    def load_items(self):
        for sonata in self.SONATAS:
            sonata = sonata.replace('K', 'k')
            self.FILES += [ f'{sonata}.{mvt}' for mvt in [1, 2, 3] ]

