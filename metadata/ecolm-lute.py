class EcolmLute(JsonCorpus):

    def parse_keys(self, filename):
        # input may be B1546-10_1, B1546-10_1.mscz, B1546-10_1.musicxml, B1546-10_1.mei, B1546-10_1/info.json
        key = filename.replace('.mscz', '').replace('.musicxml', '').replace('.mei', '').replace('/info.json', '')
        assert(len(key) in range(9, 12))

        return {
            KEY: key,
            KEYS: [key, 'template']
        }
