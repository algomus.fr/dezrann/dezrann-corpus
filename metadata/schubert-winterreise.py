
### Schubert Winterreise Dataset

class SchubertWinterreise(JsonCorpus):

    ID = "schubert-winterreise"

    FILES = glob.glob('../jazz-schubert/data/SWD/songs_swd/*/??.xml')

    def parse_keys(self, filename):
        key = '/'.join(filename.split('/')[-3:]).split('.')[0]
        return {
            KEY: key,
            KEYS: [key, ALL],
        }

