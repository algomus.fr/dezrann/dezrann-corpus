{
  "corpus": {
    "id": "weimar-jazz",

    "contributors": {
      "editor": "Martin Pfleiderer, Klaus Frieler, Jakob Abesser, Wolf-Georg Zaddach, Benjamin Burkhar",
      "transcriber": "Martin Breternitz, Peter Heppner, Yvette Kneisel, Benedikt Koch, Simon Meininger, Benjamin Napravnik, Albrecht Probst, Franziska Risch, Lydia Schulz, Amelie Zimmermann, Alaa Zouiten",
      "maintainer": "Klaus Frieler <kgf@omniversum.de>, Mathieu Giraud <mathieu@algomus.fr>"
    },

    "motto": "330+ high-quality jazz transcriptions, some of them with synchronized audio, from the Jazzomat project",
    "motto:de": "330+ hochwertige Jazz-Transkriptionen aus dem Jazzomat-Projekt, einige davon mit synchronisiertem Audio",
    "motto:el": "330+ υψηλής ποιότητας μεταγραφές τζαζ, μερικές από αυτές με συγχρονισμένο ήχο, από το έργο Jazzomat",
    "motto:fr": "330+ transcriptions précises de solos de jazz, certaines synchronisées avec des enregistrements, du projet Jazzomat",
    "motto:hr": "330+ visokokvalitetnih transkripcija jazza. Neke s sinkroniziranim zvukom iz projekta Jazzomat",
    "motto:it": "330+ trascrizioni jazz di alta qualità, alcune con audio sincronizzato, dal progetto Jazzomat",
    "motto:sl": "330+ visokokakovostnih prepisov jazza, izmed katerih so nekateri usklajenim zvočnimi posnetki iz projekta Jazzomat",

    "title": "🚧 Weimar Jazz Database",
    "title:de": "🚧 Weimar Jazz Database",
    "title:el": "🚧 Weimar Jazz Database",
    "title:hr": "🚧 Zbirka weimarskog jazza",
    "title:it": "🚧 Weimar Jazz Database",
    "title:sl": "🚧 Zbirka: Weimar Jazz",
    "year": "1920-2010",

    "text": "[Improvisation](https://en.wikipedia.org/wiki/Jazz_improvisation) is the essence of jazz. It includes the spontaneous creation of solo lines or accompaniment parts, often performed over a chord grid. Listening, transcribing, analyzing, and sometimes playing great *jazz solos* are invaluable to better understand jazz music and to contribute to its perpetual dynamism.",
    "text:de": "[Improvisation](https://de.wikipedia.org/wiki/Improvisation_(Musik)#Jazz) ist das Wesen des Jazz. Sie das spontane Spiel von Melodielinien oder Begleitmustern, zumeist über einem Akkordschema. Das Zuhören, Transkribieren, Analysieren und Nachspielen großartiger *Jazz-Soli* ist von unschätzbarem Wert wenn es darum geht, Jazzmusik besser zu verstehen und spielen.",
    "text:el": "[Αυτοσχεδιασμός](https://en.wikipedia.org/wiki/Jazz_improvisation) είναι το ουσιαστικό της τζαζ. Περιλαμβάνει τη σπονταία δημιουργία σολογραμμών ή μερών συνοδείας, συχνά εκτελούμενων πάνω σε πλέγμα συγχορδιών. Η ακρόαση, η μεταγραφή, η ανάλυση και μερικές φορές η εκτέλεση μεγάλων *τζαζ σόλων* είναι ανεκτίμητες για την καλύτερη κατανόηση της μουσικής τζαζ και τη συμβολή στη συνεχή δυναμική της.",
    "text:fr": "L'improvisation est l'essence du jazz. Écouter, transcrire, analyser et parfois jouer des solos célèbres est une belle manière de mieux comprendre la musique jazz.",
    "text:hr": "[Improvizacija](https://en.wikipedia.org/wiki/Jazz_improvisation) je u samoj srži jazza. Prateći logiku prepoznatljivih akordskih progresija, melodije i pratnja najčešće su stvoreni na licu mjesta. Aktivno slušanje, transkribiranje, analiziranje, a ponekad i samo izvođenje poznatih *jazzovskih sola*, ključan su dio razumijevanja jazza te doprinose njegovom dinamizmu.",
    "text:it": "L'[improvvisazione](https://it.wikipedia.org/wiki/Improvvisazione_(musica)) è l'essenza stessa del jazz. Consiste nella creazione estemporanea di linee soliste o parti di accompagnamento, spesso eseguite su una griglia di accordi. L'ascolto, la trascrizione, l'analisi e talvolta l'esecuzione di grandi *assoli jazz* sono pratiche musicali di importanza inestimabile, che ci aiutano a comprendere meglio questo stile di musica e che contribuiscono al suo dinamismo perpetuo.",
    "text:sl": "[Improvizacija](https://en.wikipedia.org/wiki/Jazz_improvisation) je eden izmed najpogostejših principov jazza. Po logiki prepoznavnih akordskih sosledij se melodije in spremljave največkrat ustvarjajo sproti. Aktivno poslušanje, prepisovanje, analiziranje in včasih celo izvajanje slavnih *jazz solov* so ključni del razumevanja jazza.",
    "availability": "Started at the University of Music in Weimar, the [Jazzomat](https://jazzomat.hfm-weimar.de/) project studied the jazz repertoire, in particular by transcribing and analyzing 400+ solos and aligning them to recordings. The Dezrann corpus contains 330+ of these high-quality jazz transcriptions, with chords, sections, and form annotation, from which 200+ with synchronized audio. <br><br>🚧 Note that work is still ongoing on this corpus, in particular the display of scores could be improved",
    "availability:de": "Das Projekt [Jazzomat](https://jazzomat.hfm-weimar.de/) an der Hochschule für Musik in Weimar untersuchte das Jazz-Repertoire mithilfe von mit Aufnahmen alignierten Transkriptionen und Analysen von 400+ Soli. Der Dezrann-Korpus enthält 330+ hochwertige Jazz-Transkriptionen mit Akkorden und Formannotationen, von denen 200+ mit Audioaufnahmen synchronisiert sind.",
    "availability:el": "Ξεκίνησε στο Πανεπιστήμιο της Μουσικής στο Weimar, το [Jazzomat](https://jazzomat.hfm-weimar.de/) έργο μελέτησε το ρεπερτόριο της τζαζ, ιδιαίτερα με τη μεταγραφή και ανάλυση σόλων και τον συγχρονισμό τους με ηχογραφήσεις. Το Dezrann corpus περιέχει 330+ υψηλής ποιότητας μεταγραφές τζαζ, με συγχορδίες, τμήματα και αναφορά στη μορφή, από τις οποίες το 200+ με συγχρονισμένο ήχο.",
    "availability:fr": "Initié à l'Université de Weimar, le projet [Jazzomat](https://jazzomat.hfm-weimar.de/) a étudié le répertoire jazz, en particulier en retranscrivant et analysant 400+ solos et en les alignant sur des enregistrements historiques. Le corpus Dezrann contient 330+ de ces transcriptions précises de solos, avec des annotations d'accords, de sections et de forme. 200+ enregistrements synchronisés sont disponibles.",
    "availability:hr": "Projekt [Jazzomat](https://jazzomat.hfm-weimar.de/) zasnovan je na Glazbenom sveučilištu u Weimaru. Obuhvaća istraživanja jazzovskog repertoara s naglaskom na transkripciji i analizi poznatih jazz sola te njihovim usklađivanjem s dostupnim snimkama. Korpus platforme Dezrann sadrži 330+ visoko kvalitetnih transkripcija jazz glazbe s anotacijama akordskih progresija, sekcija i glazbenih oblika, a 200+ ih je sinkronizirano i opremljeno sa zvučnim snimkama.",
    "availability:it": "Avviato presso l'Università di Musica di Weimar, il progetto [Jazzomat](https://jazzomat.hfm-weimar.de/) ha studiato il repertorio jazz, in particolare trascrivendo e analizzando assoli e allineandoli alle registrazioni. Il corpus Dezrann contiene 330+ trascrizioni jazz di alta qualità, con annotazioni sugli accordi, le sezioni e la forma. 200+ pezzi sono provvisti di audio sincronizzato.",
    "availability:sl": " Sprva na Univerzi za glasbo v Weimarju, je [Jazzomat](https://jazzomat.hfm-weimar.de/) projekt preučeval jazz repertoar, zlasti s prepisovanjem in analiziranjem solov ter usklajevanjem z njihovimi posnetki. Dezrann korpus vsebuje 330+ visokokakovostnih prepisov jazza, z akordi, odseki in označevanjem oblike, od katerih je 200+ usklajenih s pripadajočimi zvočnimi posnetki.",

    "image": "https://jazzomat.hfm-weimar.de/_images/wjazzd_logo.png",
    "image:card": "https://ws.dezrann.net/resources/corpus/images/WJD-zoom.jpg",
    "genre": "Jazz",
    "license": "ODbL",

    "quality:corpus": 2,
    "quality:corpus:metadata": 4,

    "ref": "https://jazzomat.hfm-weimar.de",
    "ref:isbn": "9783959831246",
    "showcase": [
      "0070"
    ],
    "status": "🚧 Work on this corpus is still ongoing to improve the integration into Dezrann. The rendering of scores could be improved with a better extraction from WJD internal data. The synchronization is sometimes off."  
  },
  "settings": {
    "access": "public"
  }
}