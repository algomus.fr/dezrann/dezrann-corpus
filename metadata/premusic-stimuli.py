from re import search

class PremusicStimuli(JsonCorpus):

    def parse_keys(self, filename):
        # input may be 24_70_La_ballade_Nord_Irlandaise,
        #              24_70_La_ballade_Nord_Irlandaise.mscz,
        #              24_70_La_ballade_Nord_Irlandaise.musicxml,
        #              24_70_La_ballade_Nord_Irlandaise.mei,
        #              24_70_La_ballade_Nord_Irlandaise/info.json
        key = filename.replace('.mscz', '').replace('.musicxml', '').replace('.mei', '').replace('/info.json', '')

        assert(search("[0-9]+_[0-9]+_[a-zA-Z]+.*", key))

        index_of_first_letter = key.find(next(filter(str.isalpha, key))) # from https://stackoverflow.com/a/60161972
        piece_id = key[index_of_first_letter:]

        return {
            KEY: key,
            KEYS: [key, 'template'],
            'piece_id': piece_id
        }

    FILES = [
        '24_70_La_ballade_Nord_Irlandaise',
        '24_74_Dodo_l_enfant_do',
        '24_79_Kinderszenen_Shumann',
        '24_81_Les_petits_poissons',
        '24_83_Maman_les_p_tits_bateaux',
        '24_86_Savez_vous_planter_les_choux',
        '24_90_Salut_d_amour',
        '24_92_Vive_le_vent',
        '34_82_Colchiques_dans_les_pres',
        '34_86_Un_canard',
        '34_89_Douce_nuit',
        '34_89_Duerme_te_nino',
        '34_90_Berceuse_de_Brahms',
        '34_91_Berceuse_cosaque',
        '34_93_La_tendresse',
        '34_108_Khooneye_Ma',
        '44_74_Belle_lune_belle',
        '44_77_A_la_claire_fontaine',
        '44_80_Faded',
        '44_86_A_la_volette',
        '44_87_Frere_Jacques',
        '44_88_Wiegenlied_Schubert',
        '44_92_La_marche_des_rois',
        '44_96_Jean_Petit_qui_danse',
        '68_74_Toutouig',
        '68_76_Mon_pere_m_a_donne_un_mari',
        '68_79_Aux_marches_du_palais',
        '68_79_En_passant_par_la_Lorraine',
        '68_81_Berceuse_de_Mozart',
        '68_88_Lonely_day',
        '68_89_Il_pleut_il_pleut_bergere',
        '68_90_Cadet_Rousselle'
    ]
